FROM golang:1.19

COPY . .

RUN go build main.go

ENTRYPOINT [ "./main" ]