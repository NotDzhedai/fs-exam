package main

import (
	"errors"
	"fmt"
	"strings"
)

/*

Серко В.С
КН-22м

Задача №1

*/

// get tag
func validParenthesis(input string) (string, error) {
	if input[0] != '<' {
		return "", errors.New("missing '<' at start")
	}
	var tag string
	for i := 1; i < len(input); i++ {
		if input[i] == '<' {
			return "", errors.New("tag: double '<' in tag")
		}
		if input[i] == '>' {
			return tag, nil
		}
		tag = tag + string(input[i])
	}
	return "", errors.New("tag: error in tag")
}

// attr validation
func isValidAttr(input string) error {
	// check if _ in attr
	isSpace := strings.ContainsAny(input, " ")
	if isSpace {
		return errors.New("attr: contains spaces")
	}

	// is equal sign on right spot
	attrFields := strings.Split(input, "=")
	if len(attrFields) != 2 {
		return errors.New("attr: invalid place for '='")
	}

	// is attr value in ""
	countOfQuotationMarks := strings.Count(attrFields[1], "\"")
	if countOfQuotationMarks != 2 {
		return errors.New("attr: incalid attribute value formatting")
	}

	return nil
}

// xml validation
func isValidXML(input string) error {
	var i int
	var stack []string

	for i < len(input) {
		if input[i] == '<' {
			tag, err := validParenthesis(input[i:])
			if err != nil {
				return err
			}

			// tags can't be empty
			if tag == "" || tag == "/" {
				return errors.New("tags cannot be empty")
			}

			// <tag /> - must be valid
			if tag[len(tag)-1] == '/' {
				return nil
			}

			// If we find a closing tag </tag>
			if tag[0] == '/' {

				// no opening tag
				if len(stack) == 0 {
					return errors.New("missing opening tag")
				}

				// mismatch of open and closing tags
				if stack[len(stack)-1] != tag[1:] {
					return errors.New("mismatch of open and closing tags")
				}

				// if there is a match to our closing tag then pop it from stack
				stack = stack[:len(stack)-1] // pop in golang :)
			} else {
				// if this is the opening tag then add it to stack
				// check for attr
				tagFields := strings.Split(tag, " ")
				if len(tagFields) == 1 {
					// if attr not in tag
					stack = append(stack, tag)
				} else {
					// if attr in tag
					// check if valid attr
					if err := isValidAttr(tagFields[1]); err != nil {
						return err
					}
					stack = append(stack, tagFields[0])
				}
			}
			// tag + "<" and ">". len(tag) + 2
			i = i + len(tag) + 2
		} else if input[i] == '>' {
			// random > in input
			return errors.New(" there is a random > in input")
		} else {
			i++
		}
	}

	if len(stack) > 0 {
		// missing closing tag
		return errors.New("missing closing tag")
	}

	return nil
}

func main() {
	tests := []string{
		"text",                         // true
		"text<a>more text</a>",         // true
		"text</a>",                     //false
		"<invalid<>text</invalid>",     //false
		"<abc>",                        //false
		"<a>text<b>other text</b></a>", // true
		"<a>text<b>other text</a></b>", //false
		"<> </>",                       //false
		"<tag>hello<tag2>hello</tag2><tag3></tag3></tag>", // true
		"<tag />",                // true
		"<tag attr='a\"></tag>",  // false
		"<tag attr-\"a\"></tag>", // false
		"<tag attr-'a'></tag>",   // false
		"<tag attr></tag>",       // false
		"<tag 'a'></tag>",        // false
		"<tag \"\"></tag>",       // false
	}

	for _, test := range tests {
		err := isValidXML(test)
		if err != nil {
			fmt.Printf("Case: %s - error: %v \n", test, err)
		} else {
			fmt.Printf("Case: %s - valid xml \n", test)
		}
	}
}
